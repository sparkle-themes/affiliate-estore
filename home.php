<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Affiliate_eStore
 */

get_header();
?>
<main id="primary" class="site-main d-flex">

    <div class="category-list">
        <?php do_action('affiliate_estore_woo_category'); ?>
    </div>
    
    <ul class="product-list woocommerc products">

        <?php 
            $product_args = array(
                'post_type'      => 'product',
                'post_status'           => 'publish',
            );

            $query = new WP_Query( $product_args );
            if($query->have_posts()):
                while($query->have_posts()): $query->the_post();
                    wc_get_template_part( 'content', 'product' );
                endwhile;
            endif;
            wp_reset_postdata();
        ?>
        
    </div>

</main>


<?php
get_sidebar();
get_footer();