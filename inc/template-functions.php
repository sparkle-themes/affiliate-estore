<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package Affiliate_eStore
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function affiliate_estore_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'affiliate_estore_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function affiliate_estore_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">', esc_url( get_bloginfo( 'pingback_url' ) ) );
	}
}
add_action( 'wp_head', 'affiliate_estore_pingback_header' );


if( !function_exists("affiliate_estore_woo_category")){
	function affiliate_estore_woo_category(){
		$orderby = 'name';
		$order = 'asc';
		$hide_empty = false ;
		$cat_args = array(
			'parent' => 0,
			'orderby'    => $orderby,
			'order'      => $order,
			'hide_empty' => $hide_empty,
		);
		
		$product_categories = get_terms( 'product_cat', $cat_args );
		
		if( !empty($product_categories) ){
			echo '<ul class="affliate-sidebar-cat affliate-ajax-load">';
			foreach ($product_categories as $key => $category) {
				$count_p = $category->count;
				echo '<li>';
					echo '<a href="'.get_term_link($category).'" data-cat="'.  esc_attr( $category->term_id ).'">';
						echo $category->name. ' ('.$count_p .')';
					echo '</a>';
				

				$child_arguments = array( 'parent' => $category->term_id, 'hide_empty' => false );
				$child_terms = get_terms( 'product_cat', $child_arguments );

				if( $child_terms):
					echo '<ul>';
						foreach( $child_terms as $child_term ) {
							$cat_count = 0;
							$cat_count = $child_term->name .' ('. $child_term->count. ")";
							echo '<li class="child_cat">
								<a href="'.get_term_link( $child_term, "product_cat" ).'" data-cat="'.esc_attr( $child_term->term_id ).'">'
								.$cat_count.
								'</a></li>'; // Child term
						}
					echo '</ul>';
				endif;

				echo '</li>';
			}
			echo '</ul>';
		}
	}
}
add_action( 'affiliate_estore_woo_category', 'affiliate_estore_woo_category' );