<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package Affiliate_eStore
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)
 * @link https://github.com/woocommerce/woocommerce/wiki/Declaring-WooCommerce-support-in-themes
 *
 * @return void
 */
function affiliate_estore_woocommerce_setup() {
	add_theme_support(
		'woocommerce',
		array(
			'thumbnail_image_width' => 150,
			'single_image_width'    => 300,
			'product_grid'          => array(
				'default_rows'    => 3,
				'min_rows'        => 1,
				'default_columns' => 4,
				'min_columns'     => 1,
				'max_columns'     => 6,
			),
		)
	);
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'affiliate_estore_woocommerce_setup' );

/**
 * WooCommerce specific scripts & stylesheets.
 *
 * @return void
 */
function affiliate_estore_woocommerce_scripts() {
	wp_enqueue_style( 'affiliate-estore-woocommerce-style', get_template_directory_uri() . '/woocommerce.css', array(), _S_VERSION );

	$font_path   = WC()->plugin_url() . '/assets/fonts/';
	$inline_font = '@font-face {
			font-family: "star";
			src: url("' . $font_path . 'star.eot");
			src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
				url("' . $font_path . 'star.woff") format("woff"),
				url("' . $font_path . 'star.ttf") format("truetype"),
				url("' . $font_path . 'star.svg#star") format("svg");
			font-weight: normal;
			font-style: normal;
		}';

	wp_add_inline_style( 'affiliate-estore-woocommerce-style', $inline_font );
}
add_action( 'wp_enqueue_scripts', 'affiliate_estore_woocommerce_scripts' );

/**
 * Disable the default WooCommerce stylesheet.
 *
 * Removing the default WooCommerce stylesheet and enqueing your own will
 * protect you during WooCommerce core updates.
 *
 * @link https://docs.woocommerce.com/document/disable-the-default-stylesheet/
 */
add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param  array $classes CSS classes applied to the body tag.
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function affiliate_estore_woocommerce_active_body_class( $classes ) {
	$classes[] = 'woocommerce-active';

	return $classes;
}
add_filter( 'body_class', 'affiliate_estore_woocommerce_active_body_class' );

/**
 * Related Products Args.
 *
 * @param array $args related products args.
 * @return array $args related products args.
 */
function affiliate_estore_woocommerce_related_products_args( $args ) {
	$defaults = array(
		'posts_per_page' => 3,
		'columns'        => 3,
	);

	$args = wp_parse_args( $defaults, $args );

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'affiliate_estore_woocommerce_related_products_args' );

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

if ( ! function_exists( 'affiliate_estore_woocommerce_wrapper_before' ) ) {
	/**
	 * Before Content.
	 *
	 * Wraps all WooCommerce content in wrappers which match the theme markup.
	 *
	 * @return void
	 */
	function affiliate_estore_woocommerce_wrapper_before() {
		?>
			<main id="primary" class="site-main">
		<?php
	}
}
add_action( 'woocommerce_before_main_content', 'affiliate_estore_woocommerce_wrapper_before' );

if ( ! function_exists( 'affiliate_estore_woocommerce_wrapper_after' ) ) {
	/**
	 * After Content.
	 *
	 * Closes the wrapping divs.
	 *
	 * @return void
	 */
	function affiliate_estore_woocommerce_wrapper_after() {
		?>
			</main><!-- #main -->
		<?php
	}
}
add_action( 'woocommerce_after_main_content', 'affiliate_estore_woocommerce_wrapper_after' );

/**
 * Sample implementation of the WooCommerce Mini Cart.
 *
 * You can add the WooCommerce Mini Cart to header.php like so ...
 *
	<?php
		if ( function_exists( 'affiliate_estore_woocommerce_header_cart' ) ) {
			affiliate_estore_woocommerce_header_cart();
		}
	?>
 */

if ( ! function_exists( 'affiliate_estore_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	function affiliate_estore_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		affiliate_estore_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}
add_filter( 'woocommerce_add_to_cart_fragments', 'affiliate_estore_woocommerce_cart_link_fragment' );

if ( ! function_exists( 'affiliate_estore_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function affiliate_estore_woocommerce_cart_link() {
		?>
		<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'affiliate-estore' ); ?>">
			<?php
			$item_count_text = sprintf(
				/* translators: number of items in the mini cart. */
				_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'affiliate-estore' ),
				WC()->cart->get_cart_contents_count()
			);
			?>
			<span class="amount"><?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?></span> <span class="count"><?php echo esc_html( $item_count_text ); ?></span>
		</a>
		<?php
	}
}

if ( ! function_exists( 'affiliate_estore_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function affiliate_estore_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php affiliate_estore_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
				$instance = array(
					'title' => '',
				);

				the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}


remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');

function affiliate_estore_woocommerce_template_loop_product_thumbnail(){ ?>

    <div class="store_products_item">
        <div class="store_products_item_body">
			<?php
		        global $post, $product, $product_label_custom; 

		        $sale_class = '';
		        if( $product->is_on_sale() == 1 ){
					$sale_class = 'new_sale';
				}
			?>
			<div class="flash <?php echo esc_attr( $sale_class ); ?>">

				<?php 
		            if ($product_label_custom != ''){

						echo '<span class="onnew"><span class="text">'.esc_html__('New','affiliate-estore').'</span></span>';
					}

		            if ( $product->is_on_sale() ) :

		             	echo apply_filters( 'woocommerce_sale_flash', '<span class="onlineestore_sale_label"><span class="text">' . esc_html__( 'Sale!', 'affiliate-estore' ) . '</span></span>', $post, $product );
		            
					endif;
	            ?>
			</div>

            <a href="<?php the_permalink(); ?>" class="store_product_item_link">
				<?php the_post_thumbnail('woocommerce_thumbnail'); #Products Thumbnail ?>
            </a>
        </div>
    </div>    
  	<?php 
}
add_action( 'woocommerce_before_shop_loop_item_title', 'affiliate_estore_woocommerce_template_loop_product_thumbnail', 10 );

remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );

if ( !function_exists('affiliate_estore_woocommerce_shop_loop_item_title') ) {

    function affiliate_estore_woocommerce_shop_loop_item_title(){ ?>

        <div class="store_products_item_details">
			<div class="content-wrapper">
				<h3>
					<a class="onlineestore_products_title" href="<?php the_permalink(); ?>">
						<?php the_title( ); ?>
					</a>
				</h3>
				<p><?php the_excerpt( ) ?> </p>

				<?php $product = wc_get_product( get_the_ID() ); /* get the WC_Product Object */ ?>
				<p><?php echo $product->get_price_html(); ?></p>

				<div class="product-cat-and-date">
				<?php
					$posted = get_the_time('U');
					echo human_time_diff($posted,current_time( 'U' )). " ago";
					?>
				</div>
			</div>

			<ul class="action-btns">
				
				<li class="products_item_info">
				<?php
					/**
					* woocommerce_template_loop_add_to_cart
					*/
					woocommerce_template_loop_add_to_cart();
				?>
				</li>
			</ul>
      <?php 
    }
}
add_action( 'woocommerce_shop_loop_item_title', 'affiliate_estore_woocommerce_shop_loop_item_title', 10 );



/** shop page / archive page */
add_action( 'woocommerce_before_shop_loop', 'affliate_estore_archive_item', 40 );
if( !function_exists( 'affliate_estore_archive_item' )){
	function affliate_estore_archive_item(){
		echo "<div class='content-sidebar-wrapper d-flex'>";
		echo "<div class='category-list'>";
		do_action('affiliate_estore_woo_category');
		echo "</div>";
	}
}

if ( ! function_exists( 'affiliate_estore_woocommerce_product_columns_wrapper' ) ) {
    /**
     * Product columns wrapper.
     *
     * @return  void
     */
    function affiliate_estore_woocommerce_product_columns_wrapper() {

        // echo '<div class="wrapper">';
    }
}
add_action( 'woocommerce_before_shop_loop', 'affiliate_estore_woocommerce_product_columns_wrapper', 40 );

if ( ! function_exists( 'affiliate_estore_woocommerce_product_columns_wrapper_close' ) ) {
    /**
     * Product columns wrapper close.
     *
     * @return  void
     */
    function affiliate_estore_woocommerce_product_columns_wrapper_close() {

        // echo '</div>';
        echo '</div>';
    }
}
add_action( 'woocommerce_after_shop_loop', 'affiliate_estore_woocommerce_product_columns_wrapper_close', 8 );


if ( !function_exists('affliate_estore_cat_products') ) :
	/**
     * Online eStore Category Products Ajax Function for Tabs
     *
     * @return array();
     */
    function affliate_estore_cat_products() {
		$cat_id = 0;
    	if ( isset( $_POST['cat_id'] ) ) {
       		$cat_id = intval( wp_unslash( $_POST['cat_id'] ) );
       	}

        ob_start();

		$product_args = array(
			'post_type'      => 'product',
			'post_status'           => 'publish',
			'tax_query'             => array(
				array(
					'taxonomy'      => 'product_cat',
					'field' => 'term_id', //This is optional, as it defaults to 'term_id'
					'terms'         => $cat_id,
					'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
				),
				array(
					'taxonomy'      => 'product_visibility',
					'field'         => 'slug',
					'terms'         => 'exclude-from-catalog', // Possibly 'exclude-from-search' too
					'operator'      => 'NOT IN'
				)
			)
		);

		$query = new WP_Query( $product_args );
		if($query->have_posts()):
			while($query->have_posts()): $query->the_post();
				wc_get_template_part( 'content', 'product' );
			endwhile;
		else:
			echo '<h1 class="page-title">Nothing Found</h1>';
		endif;
		wp_reset_postdata();
        
		$content = ob_get_contents();

		ob_get_clean();

		echo wp_kses_post ( $content );

		die();
    }

endif;

add_action('wp_ajax_affliate_estore_cat_products', 'affliate_estore_cat_products');
add_action('wp_ajax_nopriv_affliate_estore_cat_products', 'affliate_estore_cat_products');
