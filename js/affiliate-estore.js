jQuery(document).ready(function ($) {

    /** ajax category call back */
    $('ul.affliate-sidebar-cat.affliate-ajax-load a').on('click', function (e) {
        e.preventDefault();
        $('ul.affliate-sidebar-cat.affliate-ajax-load a').removeClass('active');
        $(this).addClass('active');

        var link = $(this).attr('href'),
            cat_id = $(this).data('cat');
        var placeholder = $('.category-list + .products');
        console.log(cat_id, link);

        $.ajax({
            url: affiliate_estore_ajax_action.ajaxurl,
            data: {
                action: 'affliate_estore_cat_products',
                cat_id: cat_id,
            },
            type: 'post',
            beforeSend: function () {
                $(placeholder).addClass('loading');
            },
            success: function (res) {
                $(placeholder).removeClass('loading');
                $(placeholder).html(res);

                window.history.replaceState('Object', 'Title', link);
            }
        });

    });
});